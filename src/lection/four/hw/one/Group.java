package lection.four.hw.one;

import lection.four.hw.one.comparators.StudentLastNameComparator;
import lection.four.hw.one.exceptions.GroupOverflowException;
import lection.four.hw.one.exceptions.StudentNotFoundException;
import lection.four.hw.one.student.Student;

import java.util.Arrays;
import java.util.Comparator;

public class Group {

    private Student[] students = new Student[10];
    private String groupName;

    public Group(String groupName, Student[] students) {
        super();
        this.groupName = groupName;
        this.students = students;
    }

    public Group(String groupName) {
        super();
        this.groupName = groupName;
    }

    public Group() {
        super();
    }

    public static void sortStudentsByLastName(Student[] students) {
        Arrays.sort(students, Comparator.nullsLast(new StudentLastNameComparator()));
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public Student[] getStudents() {
        return students;
    }

    public void setStudents(Student[] students) {
        this.students = students;
    }

    public void addStudent(Student student) throws GroupOverflowException {
        for (int i = 0; i < this.students.length; i++) {
            if (this.students[i] != null && i == this.students.length - 1) {
                throw new GroupOverflowException(student.getName() + " " + student.getLastName() +
                        " cannot be added to the " + this.groupName + " group as it is full!");
            } else if (this.students[i] == null) {
                student.setGroupName(this.groupName);
                this.students[i] = student;
                System.out.println("\u001B[32m" +
                        student.getName() + " " + student.getLastName() +
                        " added to " + this.groupName + " group." + "\u001B[0m");
                break;
            }
        }
    }

    public Student searchStudentByLastName(String lastName) throws StudentNotFoundException {
        Student foundStudent = null;
        for (Student student : this.students) {
            if (student != null && student.getLastName().equals(lastName)) {
                foundStudent = student;
            }
        }
        if (foundStudent == null) {
            throw new StudentNotFoundException("Student with last name " + lastName + " not found " + this.groupName + " group.");
        }

        return foundStudent;
    }

    public boolean removeStudentByID(int id) {
        for (int i = 0; i < this.students.length; i++) {
            if (this.students[i] != null && this.students[i].getId() == id) {
                this.students[i] = null;
                System.out.println("Student with student card id " + id + " is removed from group " + this.groupName);
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        sortStudentsByLastName(students);
        StringBuilder sb = new StringBuilder();

        sb.append("Group:\n");
        sb.append("  groupName = '").append(groupName).append("'\n");

        for (Student student : this.students) {
            sb.append("    ").append(student != null ? student.toString() : "null").append("\n");
        }

        return  sb.toString();
    }
}
