package lection.four.hw.one;

import lection.four.hw.one.exceptions.GroupOverflowException;
import lection.four.hw.one.student.CSVStringConverter;
import lection.four.hw.one.student.Student;
import lection.four.hw.one.student.StudentBuilder;

public class Main {
    public static void main(String[] args) {

        String groupName = "Journalism";
        Group group = new Group(groupName);

        Student student01 = StudentBuilder.createStudent();
        try {
            StudentBuilder.addToGroup(student01, group);
        } catch (GroupOverflowException e) {
            System.out.println("\u001B[31m" + e.getMessage() + "\u001B[0m");
        }

        CSVStringConverter csv = new CSVStringConverter();
        String studentCSV = csv.toStringRepresentation(student01);
        Student student02 = csv.fromStringRepresentation(studentCSV);

        try {
            StudentBuilder.addToGroup(student02, group);
        } catch (GroupOverflowException e) {
            System.out.println("\u001B[31m" + e.getMessage() + "\u001B[0m");
        }

        printGroupInfo(group);
    }

    private static void printGroupInfo(Group group) {
        System.out.println("\u001B[34m" + "-------------------------------------------" + "\u001B[0m");
        System.out.println("\u001B[34m" + group + "\u001B[0m");
        System.out.println("\u001B[34m" + "-------------------------------------------" + "\u001B[0m");
    }
}